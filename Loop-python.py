#first version:

if __name__ == '__main__':
    n = int(input())
    for i in range(0,n):
        print(i*i)

#second version with added input place

if __name__ == '__main__':
    n = int(input("Type a number"))
    for i in range(0,n):
        print(i*i)
       