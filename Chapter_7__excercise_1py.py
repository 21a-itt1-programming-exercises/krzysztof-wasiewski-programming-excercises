#Excercise 1, chapter 7 
#Exercise 1: Write a program to read through a file and print the contents
#of the file (line by line) all in upper case. 

fhand = open('mbox-short.txt')
for line in fhand:
  line = line.rstrip()
  print(line.upper())
