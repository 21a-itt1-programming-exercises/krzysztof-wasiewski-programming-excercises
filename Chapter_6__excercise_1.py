#Exercise 1: Write a while loop that starts at the last character in the string and works its way backwards 
#to the first character in the string, printing each letter on a separate line, except backwards.

word = input("Enter a word:")
index = -1

negative_len = len(word) * -1

while True:
  if index == negative_len - 1: 
    break
  letter = word[index]
  print(letter)
  index = index - 1

  #Exercise 2: Given that fruit is a string, what does fruit[:] mean?
    #fruit[:] is the whole string stored in the variable called fruit

