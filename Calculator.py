#Requirements for the calculator program are:
#Define 4 functions to add, subtract, divide or multiply 2 float numbers
#If the user inputs done at any time the program will stop and print the message goodbye and exit
#If the user enters anything else than numbers or done the program should catch a ValueError and print the message only numbers and "done" is accepted as input, please try again and then restart
#If the user tries to divide by zero the program should catch a ZeroDivisionError and print the message cannot divide by zero, please try again and then restart
#On successful calculation the program should restart and ask the user to enter new numbers again

def add(x, y):
    return x + y

def subtract(x, y):
    return x - y

def multiply(x, y):
    return x * y

def divide(x, y):
    return x / y

print("Would like to: ")
print("1 - Add")
print("2 - Subtract")
print("3 - Multiply")
print("4 - Divide")

while True:
    choice = input("Enter choosed function(1/2/3/4): ")
    if choice in ('1', '2', '3', '4'):
        number1 = float(input("Enter first number: "))
        number2 = float(input("Enter second number: "))

    if choice == "done":
            print ("goodbye")
            break

    elif choice == '1':
            print(number1, "+", number2, "=", add(number1, number2))

    elif choice == '2':
            print(number1, "-", number2, "=", subtract(number1, number2))

    elif choice == '3':
            print(number1, "*", number2, "=", multiply(number1, number2))

    elif choice == '4':
            if number2 == 0:
                print("Cannot divide by zero, please try again")
                continue
            else:
                print(number1, "/", number2, "=", divide(number1, number2))

    if choice == "done":
            print ("goodbye")
            break
        
    else:
        print("Only numbers and 'done' are accepted as input, please try again")