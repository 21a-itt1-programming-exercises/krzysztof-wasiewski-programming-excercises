#Exercise  9.3: Write a program to read through a mail log, build a histogramusing a dictionary to 
# count how many messages have come from each emailaddress, and print the dictionary.
# Example file: mbox-short.txt

dictionary_addresses = dict()                  
fname = input('Enter file name: ')
try:
    fhand = open(fname)
except FileNotFoundError:
    print('File cannot be opened:', fname)
    exit()

for line in fhand:
    words = line.split()
    if len(words) < 2 or words[0] != 'From':
        continue
    else:
        if words[1] not in dictionary_addresses:
            dictionary_addresses[words[1]] = 1  
        else:
            dictionary_addresses[words[1]] += 1     
print(dictionary_addresses)