# Exercise  8.6: Rewrite the program that prompts the user for a list of numbers and prints out the maximum and minimum of the numbers at the end when the user
# enters "done". Write the program to store the numbers the user enters in a list and use the max() and min() functions to compute the maximum and minimum
# numbers after the loop completes.

my_list = []                    
while True:
    number = 0.0
    print ("To stop program type: done")
    print("Put only numbers between 1 and 9!")
    user_input = input('Please enter a number: ')
    if user_input == 'done':
        break
    try:
        number = float(user_input)
    except ValueError:
        print('Invalid input, please restart the program.')
        quit()
    my_list.append(user_input)

print('Maximum value is: ', max(my_list))
print('Minimum value is: ', min(my_list))