#Exercise  8.1: Write a function called chop that takes a list and modifies it, removing the first and last elements, and returns None.
#Then write a function called middle that takes a list and returns a new listthat contains all but the first and last elements.

def chop(list):
    del list[0]
    del list[-1]

def middle(list):
    new = list [1:]
    del new [-1]
    return new

my_list = [1,2,3,4]
my_list2 = [1,2,3,4]

chop_list = chop(my_list)
print (my_list)
print (chop_list)

middle_list = middle (my_list2)
print (my_list2)
print (middle_list)