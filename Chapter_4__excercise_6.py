def computepay():
    try:
        hours = int(input("Enter Hours: "))
        rate = int(input("Enter Rate: "))
        if hours <= 40:
            print(hours * rate, "$ - This is your pay")
        elif hours > 40:
            
            print((hours * rate)+(hours - 40) * (rate * 0.5), "$ - This is your pay")
    except:
        print("Error, please enter ONLY numeric input. Run program again.\n")
computepay()