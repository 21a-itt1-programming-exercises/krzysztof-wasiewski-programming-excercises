# Exercise  11.2: Write a program to look for lines of the form 'New Revision: 39771' and extract the number from each of the lines 
# using a regular expression and the findall() method. Compute the average of the numbers and print out the average.

import re
fname = input("Enter File Name")
fhand = open (fname)
count = [0]
delimeter = " "
for line in fhand:
    line = line.rstrip()
    x = re.findall('^Details:.rev+([0-9]+)', line)
    if len(x) > 0:
        s = delimeter.join(x)
        print(s)
