import random
import time

def check_guess(question, answer):
    global score
    still_guessing = True
    attempt = 0
    while still_guessing and attempt < 2:
        if question.lower() == answer.lower():
            print("Correct Answer")
            score = score + 1
            still_guessing = False
        else:
            if attempt < 1:
                question = input("Sorry Wrong Answer, please try again: ")
            attempt = attempt + 1
    if attempt == 2:
        print("The Correct answer is ",answer )

    
score = 0 # at the begging score is 0, we will gain points by anwsering questions correctly
print("GENERAL KNOWLEDGE QUIZ")
print (" \n-RULES")
print("- There will be ten questions to anwser in Part 1 of the quiz.\n- You have two tries for each question.")
print("- If your score will be above 8 you will go to the second part of the quiz.")

question1 = input("1. What is the capitol city of Italy?\n ")
check_guess(question1, "Rome")
question2 = input("2. How much is 21x37?\n ")
check_guess(question2, "777")
question3 = input("3. Which animals always have their's houses with them?\n ")
check_guess(question3, "Snails")
question4 = input("4. What is the most popular electric car brand?\n ")
check_guess(question4, "Tesla")
question5 = input("5. Which is the larget land animal?\n ")
check_guess(question5, "Elephant")
question6 = input("6. What was founded first? Apple or Microsoft or Google?\n ")
check_guess(question6, "Apple")
question7 = input("7. What is the largest country on Earth?\n ")
check_guess(question7, "Russia")
question8 = input("8. What do you put into keyhole?\n ")
check_guess(question8, "Key")
question9 = input("9. What was the name of the first president of the USA ..... Washington?\n ")
check_guess(question9, "George")
question10 = input("10. What is the biggest ocean in the world? The .... Ocean\n ")
check_guess(question10, "Pacific")

print("Your Score is "+ str(score))
time.sleep(4)

if score >8:
    def check_guess(question, answer):
        global score2
        still_guessing = True
        attempt = 0
        while still_guessing and attempt < 2:
            if question.lower() == answer.lower():
                print("Correct Answer")
                score2 = score2 + 1
                still_guessing = False
            else:
                if attempt < 1:
                    question = input("Sorry Wrong Answer, please try again: ")
                attempt = attempt + 1
        if attempt == 2:
            print("The Correct answer is ",answer )

    score2 = 0
    print (" \nRound 2 out of 2\nIf your total score will be above 15, you will recive a reward.\nGood luck!\n")
    time.sleep(6)
    question11 = input("11. What is third Italian colour? Red, green and ...\n ")
    check_guess(question11, "White")
    question12 = input("12. How much is 22x22?\n ")
    check_guess(question12, "444")
    question13 = input("13. Which insect is a symbol of luck?\n ")
    check_guess(question13, "ladybug")
    question14 = input("14. Who attacked Poland first during WW2? Russia or Germany\n ")
    check_guess(question14, "Germany")
    question15 = input("15. What is capitol the city of Brasil?\n ")
    check_guess(question15, "Brasil")
    question16 = input("16. HUman is made in 60-70% out of...?\n ")
    check_guess(question16, "water")
    question17 = input("17. What is other name for USB Stick?\n ")
    check_guess(question17, "Pendrive")
    question18 = input("18. Used mostly in chemistry, show informations about elements. .... Table?\n ")
    check_guess(question18, "Periodic")
    question19 = input("19. What was the name of the USA inteligence agency?\n ")
    check_guess(question19, "CIA")
    question20 = input("20. What is the biggest online shopping platform?\n ")
    check_guess(question20, "Amazon")

    total_score = score + score2

    if total_score>15:

        print("Your Score is "+ str(total_score))
        print ("You are a great player!\nAs a reword you will recive a random story wrote by that python program!\n")
        time.sleep(5)

        Sentence_starter = ['About 100 years ago', 'In the 2137 BC', 'Two years ago', 'Once upon a time', 'During World War 2']
    first_character = [' there was a soldier.', ' there lived a prince.',' there was a man named Leonardo.', ' there lived a farmer.']
    date = [' One day', ' One full-moon night']
    plot = [' he was passing by ',' he was going for a picnic to ']
    place = [' the mountains', 'the garden', 'the castle', 'the cave', 'the road']
    second_character = [' he saw a man', ' he saw a young lady', ' he saw a troll']
    age = [' who seemed to be in late 30s ', ' who seemed to be very old ']
    doing = ['eating yellow grapes.', 'searching for something in a bag.', 'digging a well on roadside.']
    
    print(random.choice(Sentence_starter)+random.choice(first_character)+
        random.choice(date)+random.choice(plot) +
        random.choice(place)+random.choice(second_character)+
        random.choice(age)+random.choice(doing))
else: quit
